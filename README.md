#  The Movie DB Challenge

Hola. Primero de todo gracias por la oportunidad de poder hacer el test.

En el día que recibí el Challenge lo memorizé y a ratos (son días complicados y con poco tiempo) he ido haciendo. 
Al volver a leer el PDF me he dado cuenta lo del Git, y me sabe mal. En el resto de tests que he realizado no era requisito y por eso ni me lo planteé.

He montado un sistema de cache de dos páginas permanentes y dinámicamente crece, con un cleanup. 
Evidentemente en un proyecto en producción hay que madurar la gestión de memória, hits, cleanup, etc, pero creo que es suficiente
para que podáis evaluar.

De esta forma permite subir y bajar por toda la lista sin problemas.

Un saludo,

Dani



