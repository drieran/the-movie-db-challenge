//
//  WaitingForMoreCell.swift
//  The Movie DB Challenge
//
//  Created by Daniel Riera Flinch on 22/02/2019.
//  Copyright © 2019 Daniel Riera Flinch. All rights reserved.
//

import UIKit

class WaitingForMoreCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
