//
//  Movie.swift
//  The Ugly Music Player
//
//  Created by Daniel Riera Flinch on 20/02/2019.
//  Copyright © 2019 Daniel Riera Flinch. All rights reserved.
//

import Foundation

struct Movie {
    public let preview: String?
    public let title: String?
    public let year: Int?
    public let overview: String?
}

