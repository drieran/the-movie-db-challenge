//
//  MoviesProviderWithApple.swift
//  The Ugly Music Player
//
//  Created by Daniel Riera Flinch on 20/02/2019.
//  Copyright © 2019 Daniel Riera Flinch. All rights reserved.
//

import Foundation

// TODO: Implement a cache. Is the typical scenario where data no changes in hours and users repeats search.
// TODO: If is a paid API, is a good scenario to bridge requests in backend and implement a global cache.

class MoviesProviderWithMock: MoviesProviderContract {
    static let shared = MoviesProviderWithMock()
    
    private var lastError = ""
    
    private init() {}
    
    func load(with filter: String, page: Int, cancelPrevious: Bool, result: @escaping (Bool, [Movie]?, Int, Int) -> Void) {
       
        var retMovies: [Movie] = []
        var pos = 0
        let pages = 3
        
        pos += 1
        retMovies.append(Movie(preview: "https://picsum.photos/200/300", title: "Title \(page) \(pos)", year: 2000+page+pos, overview: "blablabla adfa jdsfñ ajsdñf jasdñf  \(page) \(pos) blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  blablabla adfa jdsfñ ajsdñf jasdñf  "))
        pos += 1
        retMovies.append(Movie(preview: "https://picsum.photos/200/300", title: "Title \(page) \(pos) Title \(page) \(pos) Title \(page) \(pos)  Title \(page) \(pos) Title \(page) \(pos) ", year: 2000+page-pos, overview: "blablabla  \(page) \(pos)"))
        pos += 1
        retMovies.append(Movie(preview: "https://picsum.photos/200/300", title: "Title \(page) \(pos)", year: 2000+page, overview: "blablabla  Page:\(page) \(pos)"))
        pos += 1
        retMovies.append(Movie(preview: "https://picsum.photos/200/300", title: "Title \(page) \(pos)", year: 2000+page, overview: "blablabla  Page:\(page) \(pos)"))
        pos += 1
        retMovies.append(Movie(preview: "https://picsum.photos/200/300", title: "Title \(page) \(pos)", year: 2000+page, overview: "blablabla  Page:\(page) \(pos)"))
        pos += 1
        retMovies.append(Movie(preview: "https://picsum.photos/200/300", title: "Title \(page) \(pos)", year: 2000+page, overview: "blablabla  Page:\(page) \(pos)"))

        result(true, retMovies, pages, pages*pos-1)
    }
    
    func getLastError() -> String {
        return lastError
    }
}
