//
//  MoviesProviderWithApple.swift
//  The Ugly Music Player
//
//  Created by Daniel Riera Flinch on 20/02/2019.
//  Copyright © 2019 Daniel Riera Flinch. All rights reserved.
//

import Foundation

// TODO: Implement a cache. Is the typical scenario where data no changes in hours and users repeats search.
// TODO: If is a paid API, is a good scenario to bridge requests in backend and implement a global cache.

class MoviesProviderWithTMDB: MoviesProviderContract {
    static let shared = MoviesProviderWithTMDB()
    
    let API_KEY = "93aea0c77bc168d8bbce3918cefefa45"
    let API_VERSION = "4"
    let API_BASEURL = "https://api.themoviedb.org"
    let API_POPULAR = "discover/movie?sort_by=popularity.desc"
    let API_SEARCH = "search/movie?query="
    let API_BASEIMAGEURL = "https://image.tmdb.org/t/p/w500"
    
    // Oh Shit. Codables doesn't support inhiterance for CodingKeys :(
    struct MovieRec: Codable {
        public let preview: String?
        public let title: String?
        public let releaseDate: String?
        public let overview: String?
        
        enum CodingKeys: String, CodingKey {
            case title = "title"
            case releaseDate = "release_date"
            case overview = "overview"
            case preview = "poster_path"
        }
    }
    
    // Note: This is not a public model because is only for this data provider.
    struct MoviesRequest: Codable {
        let page: Int
        let total_results: Int
        let total_pages: Int
        var movies: [MovieRec]
        
        enum CodingKeys: String, CodingKey {
            case page = "page"
            case total_results = "total_results"
            case total_pages = "total_pages"
            case movies = "results"
        }
    }
    
    private var lastError = ""
    
    private var task: URLSessionDataTask?
    
    private init() {}
    
    func load(with filter: String, page: Int, cancelPrevious: Bool, result: @escaping (Bool, [Movie]?, Int, Int) -> Void) {
        guard let escapedFilter = filter.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed),
            let url = URL(string: "\(API_BASEURL)/\(API_VERSION)/\(escapedFilter.isEmpty ? API_POPULAR : API_SEARCH+escapedFilter)&api_key=\(API_KEY)&page=\(page)") else {
                lastError = "Malformed url or search terms."
                return result(false, nil, 0, 0)
        }

        if cancelPrevious {
            task?.cancel() // Cancel previous requests.
        }
        
        task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                // If we cancel a task, we have an error :(
                if let msg = error?.localizedDescription, msg == "cancelled" {
                    return
                }
                
                self.lastError = "Error retrieving data. \(error?.localizedDescription ?? "uknown error")"
                return result(false, nil, 0, 0)
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                self.lastError = ""
                
                if !data.isEmpty {
                    let data = try decoder.decode(MoviesRequest.self, from: data)

                    var retMovies: [Movie] = []
                    data.movies.forEach({ (m) in
                        var year: Int? = nil
                        var imageUrl: String?

                        if let date = m.releaseDate, date.count > 4 {
                            let index = date.index(date.startIndex, offsetBy: 4)
                            year = Int(String(date[..<index]))
                            if year != nil && year! < 1900 {
                                year = nil // Invalid year
                            }
                        }
                        
                        if let image = m.preview {
                            imageUrl = "\(self.API_BASEIMAGEURL)\(image)"
                        }
                        
                        retMovies.append(Movie(preview: imageUrl, title: m.title, year: year, overview: m.overview))
                    })
                    return result(true, retMovies, data.total_pages, data.total_results)
                } else {
                    return result(true, [], 0, 0)
                }
                
            } catch let err {
                self.lastError = "JSON decoder failed. \(err.localizedDescription)"
                return result(false, nil, 0, 0)
            }
        }
        
        task?.resume()
    }
    
    func getLastError() -> String {
        return lastError
    }
}
