//
//  Settings.swift
//  The Movie DB Challenge
//
//  Created by Daniel Riera Flinch on 21/02/2019.
//  Copyright © 2019 Daniel Riera Flinch. All rights reserved.
//

import Foundation

// App Settings. This is only a challenge, here is a good place!

class Settings {
    static let minimumTimeBeforeExecuteSearch = 0.5 // Minimize spam requests ;)
}
