//
//  Logger.swift
//  The Ugly Music Player
//
//  Created by Daniel Riera Flinch on 20/02/2019.
//  Copyright © 2019 Daniel Riera Flinch. All rights reserved.
//

import Foundation
import SwiftyBeaver

// Very basic logger class, only for this project.

class Logger: LoggerContract {
    static let shared = Logger()

    var log = SwiftyBeaver.self

    private init() {
        let console = ConsoleDestination()
        log.addDestination(console)
    }
    
    func debug(_ message: Any) {
        self.log.debug(message)
    }
    
    func info(_ message: Any) {
        self.log.info(message)
    }
    
    func error(_ message: Any) {
        self.log.error(message)
    }
}
