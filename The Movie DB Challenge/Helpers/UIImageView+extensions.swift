//
//  UIImage+extensions.swift
//  The Ugly Music Player
//
//  Created by Daniel Riera Flinch on 20/02/2019.
//  Copyright © 2019 Daniel Riera Flinch. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView {
    // Why this? Because we can change provider without change any line of code.
    // Today is Kingfisher, and tomorrow ...
    func setImageWithUrl(_ imageUrl: String) {
        let url = URL(string: imageUrl)
        self.kf.setImage(with: url)
    }
}
