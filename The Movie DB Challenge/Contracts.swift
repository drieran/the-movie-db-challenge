//
//  Contracts.swift
//  The Ugly Music Player
//
//  Created by Daniel Riera Flinch on 20/02/2019.
//  Copyright © 2019 Daniel Riera Flinch. All rights reserved.
//

import Foundation

protocol LoggerContract {
    func debug(_ message: Any)
    func info(_ message: Any)
    func error(_ message: Any)
}

protocol MoviesListPresenterContract {
    func search(_ searchText: String, result: ((Bool) -> Void)?)
    func getLastError() -> String
    func getCount() -> Int
    func getCurrentCount() -> Int
    func getTitle(for position: Int) -> String
    func getImageUrl(for position: Int) -> String
    func getYear(for position: Int) -> String
    func getOverview(for position: Int) -> String
    
    func isRecordAvailable(for position: Int, searchDataIfNotAvailable: Bool) -> Bool
}

protocol MoviesProviderContract {
    func load(with filter: String, page: Int, cancelPrevious: Bool, result: @escaping (Bool, [Movie]?, Int, Int) -> Void)
    func getLastError() -> String
}

