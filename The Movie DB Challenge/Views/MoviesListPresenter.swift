//
//  MoviesListPresenter.swift
//  The Ugly Music Player
//
//  Created by Daniel Riera Flinch on 20/02/2019.
//  Copyright © 2019 Daniel Riera Flinch. All rights reserved.
//

import Foundation
import UIKit

class MoviesListsPresenter: MoviesListPresenterContract {
    private var provider: MoviesProviderContract
    private var movies: [Int: [Movie]] = [:]
    private var lastViewedPages = [ -1, -1 ]
    private var lastError: String = ""
    
    private var lastSearchText = ""
    private var totalPages: Int = 0
    private var totalRecords: Int = 0
    private var recordsPerPage: Int = -1
    private var searching = 0
    
    private var executeAfterNewData: ((Bool) -> Void)?

    init(withInteractor: MoviesProviderContract) {
        self.provider = withInteractor
    }
    
    func search(_ searchText: String, result: ((Bool) -> Void)?) {
        // Each new search starts from scratch
        self.lastSearchText = searchText
        self.totalPages = 0
        self.totalRecords = 0
        self.recordsPerPage = -1
        self.executeAfterNewData = result
        self.lastViewedPages = [ -1, -1 ]
        self.movies.removeAll()

        doSearch(page: 1, cancelPrevious: true)
    }
    
    private func doSearch(page: Int, cancelPrevious: Bool) {
        guard movies[page] == nil else { return }
        
        searching += 1
        movies[page] = []
        if lastViewedPages[1] != page {
            lastViewedPages[0] = lastViewedPages[1]
            lastViewedPages[1] = page
        }
        
        self.provider.load(with: self.lastSearchText, page: page, cancelPrevious: cancelPrevious) { (success, movies, totalPages, totalMovies) in
            self.totalPages = totalPages
            self.totalRecords = totalMovies
            
            if self.recordsPerPage == -1 {
                self.recordsPerPage = movies?.count ?? 0
            }

            if !success {
                self.lastError = self.provider.getLastError()
                self.executeAfterNewData?(false)
                return
            }
            
            if let newMovies = movies {
                self.movies[page] = newMovies
            }
            
            self.executeAfterNewData?(true)
            self.searching -= 1
            
            if self.searching == 0 {
                // CleanUp
                self.movies = self.movies.filter({ $0.key == self.lastViewedPages[0] || $0.key == self.lastViewedPages[1] })
            }
        }
    }
    
    func getLastError() -> String { return lastError }
    
    // MARK: - Data related
    
    func getCount() -> Int {
        return totalRecords
    }
    
    func getCurrentCount() -> Int {
        let maxPage = max(lastViewedPages[0], lastViewedPages[1]) + 1
        let cnt = maxPage * recordsPerPage
        
        return cnt < getCount() ? cnt : getCount()
    }
    
    func getTitle(for position: Int) -> String {
        guard validPosition(position) else { return "" } // No crash, please!
        
        return getMovie(for: position)?.title ?? "Not available"
    }
    
    func getImageUrl(for position: Int) -> String {
        guard validPosition(position) else { return "" } // No crash, please!

        return getMovie(for: position)?.preview ?? ""
    }
    
    func getYear(for position: Int) -> String {
        guard validPosition(position) else { return "" } // No crash, please!
        
        if let year = getMovie(for: position)?.year, year > 1900 {
            return "\(year)"
        }
        
        return "Not available"
    }
    
    func getOverview(for position: Int) -> String {
        guard validPosition(position) else { return "" } // No crash, please!

        return getMovie(for: position)?.overview ?? "Not available"
    }
    
    func isRecordAvailable(for position: Int, searchDataIfNotAvailable: Bool) -> Bool {
        guard recordsPerPage > 0 else { return false }
        let page = Int(floor(Double(position) / Double(recordsPerPage))) + 1
        let index = position - ((page - 1) * recordsPerPage)
        var available = movies[page] != nil

        if available, index >= movies[page]!.count {
            available = false
        }

        if !available && searchDataIfNotAvailable {
            doSearch(page: page, cancelPrevious: false)
        }

        return available
    }
    
    // MARK: - Helpers
    
    private func validPosition(_ position: Int) -> Bool {
        return position >= 0 && position < getCount() && isRecordAvailable(for: position, searchDataIfNotAvailable: false)
    }
    
    private func getMovie(for position: Int) -> Movie? {
        let page = Int(floor(Double(position) / Double(recordsPerPage))) + 1
        let index = position - ((page - 1) * recordsPerPage)
        
        if let movies = movies[page] {
            if index < movies.count {
                return movies[index]
            }
        }
        
        return nil
    }
}

