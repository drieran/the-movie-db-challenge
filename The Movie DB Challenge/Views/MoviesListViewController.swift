//
//  ViewController.swift
//  The Ugly Music Player
//
//  Created by Daniel Riera Flinch on 20/02/2019.
//  Copyright © 2019 Daniel Riera Flinch. All rights reserved.
//

import UIKit
import Toast_Swift

// TODO: Usability: Implement last n searches (or top searches) and show to the user.

class MoviesListViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let presenter: MoviesListPresenterContract = MoviesListsPresenter(withInteractor: MoviesProviderWithTMDB.shared)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        activityIndicator.isHidden = true

        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600
        
        executeSearch()
    }
    
    // MARK: - SearchBar
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.executeSearch), object: nil)
        self.perform(#selector(self.executeSearch), with: nil, afterDelay: Settings.minimumTimeBeforeExecuteSearch)
    }
    
    @objc private func executeSearch() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        presenter.search(searchBar.text ?? "") { (success) in
            DispatchQueue.main.async {
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                
                if !success {
                    // Only for this test.
                    self.view.makeToast("Sorry, I can't load your music. \(self.presenter.getLastError())", duration: 3.0, position: .bottom)
                } else if self.presenter.getCount() == 0 {
                    // Only for this test.
                    self.view.makeToast("No data found for you search", duration: 1.5, position: .bottom)
                }

                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getCurrentCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !presenter.isRecordAvailable(for: indexPath.row, searchDataIfNotAvailable: true) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WaitingForMoreCell") as! WaitingForMoreCell
            cell.activityIndicator.startAnimating()
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell") as! MovieCell
        
        cell.title.text = presenter.getTitle(for: indexPath.row)
        cell.preview.setImageWithUrl(presenter.getImageUrl(for: indexPath.row))
        cell.year.text = presenter.getYear(for: indexPath.row)
        cell.overview.text = presenter.getOverview(for: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        searchBar.resignFirstResponder()
    }
}

